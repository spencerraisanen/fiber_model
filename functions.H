
arma::vec matvec (arma::mat a, arma::vec b)
{
  arma::vec r = arma::zeros<arma::vec>(3);
  r = {{a(0,0) * b(0) + a(0,1) * b(1) + a(0,2) * b(2)},
       {a(1,0) * b(0) + a(1,1) * b(1) + a(1,2) * b(2)},
       {a(2,0) * b(0) + a(2,1) * b(1) + a(2,2) * b(2)}
  };
  return r;
}