#include <random> 
//the above are needed to produce random results every time

//These values can be adjusted, they apply to the fiber 
//length, diameter, mass, segment number, and number of fibers
//note that wclean and wmake need to be 
//run after implementing any changes
scalar li(.1); 
scalar di(.01); 
scalar m(1);
scalar segments(9); 
int fiber_number(2); 

//apply to the rho and drag coefficient for cross flow over
//a circular cylinder
scalar rho(.00125); 
scalar CDI(1);     

//Common values for all fibers only dependent on length and diameter
scalar bi = ((1 / 1.24) * di * Foam::sqrt(Foam::log(li / di)));
scalar ei = (Foam::pow((1 - (sqr(bi) / sqr(li))), .5)); //ai=li
scalar Lei = (Foam::log((1 + ei) / (1 - ei)));
scalar XiA = ((8 / 3) * pow(ei, 3) * pow(-2 * ei + (1 + pow(ei, 2)) * Lei, -1));
scalar YiA = ((16 / 3) * pow(ei, 3) * pow(2 * ei + (-1 + 3 * pow(ei, 2)) * Lei, -1));
scalar XiC = ((4 / 3) * pow(ei, 3) * (1 - pow(ei, 2)) * pow(2 * ei - (1 - pow(ei, 2)) * Lei, -1));
scalar YiC = ((4 / 3) * pow(ei, 3) * (2 - pow(ei, 2)) * pow(-2 * ei - (1 + pow(ei, 2)) * Lei, -1));
scalar YiH = ((4 / 3) * pow(ei, 5) * pow(-2 * ei + (1 + pow(ei, 2)) * Lei, -1));
tensor delta(1, 0, 0, 0, 1, 0, 0, 0, 1);

/*
scalar max_length = segments * li;
std::default_random_engine de(time(0)); //seed set to be random at every moment
std::normal_distribution<float> nd(max_length * .9, max_length *.05); //mean followed by standard deviation, set arbitrarily
float rarray[fiber_number] = { 0 }; 
for (int i = 0; i < fiber_number; i++)
{
   rarray[i] = nd(de); //Generate numbers for fiber lengths, need to ensure the random numbers are random still
   
}
*/



//temporary
arma::cube zhatfinal = arma::randu<arma::cube>(3,1,9);

zhatfinal.slice(0) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(1) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(2) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(3) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(4) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(5) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(6) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(7) = arma::vec({{1}, {0}, {0}});
zhatfinal.slice(8) = arma::vec({{1}, {0}, {0}});
arma::cube rfinal = arma::ones<arma::cube>(3,1,9);
rfinal.slice(0) = arma::vec({{1 * li *2}, {1}, {1}});
rfinal.slice(1) = arma::vec({{2 * li *2}, {1}, {1}});
rfinal.slice(2) = arma::vec({{3 * li *2}, {1}, {1}});
rfinal.slice(3) = arma::vec({{4 * li *2}, {1}, {1}});
rfinal.slice(4) = arma::vec({{5 * li *2}, {1}, {1}});
rfinal.slice(5) = arma::vec({{6 * li *2}, {1}, {1}});
rfinal.slice(6) = arma::vec({{7 * li *2}, {1}, {1}});
rfinal.slice(7) = arma::vec({{8 * li *2}, {1}, {1}});
rfinal.slice(8) = arma::vec({{9 * li *2}, {1}, {1}});
arma::cube rpreviousfinal = arma::ones<arma::cube>(3,1,9);
rpreviousfinal.slice(0) = arma::vec({{1 * li}, {1}, {1}});
rpreviousfinal.slice(1) = arma::vec({{2 * li}, {1}, {1}});
rpreviousfinal.slice(2) = arma::vec({{3 * li}, {1}, {1}});
rpreviousfinal.slice(3) = arma::vec({{4 * li}, {1}, {1}});
rpreviousfinal.slice(4) = arma::vec({{5 * li}, {1}, {1}});
rpreviousfinal.slice(5) = arma::vec({{6 * li}, {1}, {1}});
rpreviousfinal.slice(6) = arma::vec({{7 * li}, {1}, {1}});
rpreviousfinal.slice(7) = arma::vec({{8 * li}, {1}, {1}});
rpreviousfinal.slice(8) = arma::vec({{9 * li}, {1}, {1}});
vector omegaold(1, 0, 0); //placeholder need to save the previous timesteps and specify the first
vector omegaold_next(1, 0, 0); //placeholder need to save the previous timesteps and specify the first

